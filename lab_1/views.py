from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Anastasia Greta Elena' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 10, 8) #TODO Implement this, format (Year, Month, Date)
npm = 1706043954 # TODO Implement this
facul = 'Computer Science'
maj = 'Information System'
hob = 'Listening to Music'
des = 'I am a perfectionist girl who loves everything about Korea :)'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'faculty': facul, 'major': maj, 'hobby': hob, 'description': des}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
